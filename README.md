# yok

[![Crates.io](https://img.shields.io/crates/v/yok.svg)](https://crates.io/crates/yok)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/yok)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/yok/-/raw/master/LICENSE)


A crate for Embed the contents of a directory in your binary at compile time.

## Getting Started
```sh
cargo add yok
```
```rust
#[allow(warnings)]
fn main() {
    use yok::{Dir, Bytes,include_dir};
    const DATA: &[u8] = include_dir();
    let dir: Dir = DATA.into_dir();
    for entry in &dir.data {
        if entry.is_file {
            println!("{}", String::from_utf8_lossy(&entry.contents));
        } else if entry.is_dir {
            println!("{}", entry.path);
        }
    }
    dir.extract("./path");
}
```
## Set YOK_PATH env and run or default path is cuttent dir
```sh
# linux,macos
export YOK_PATH="/home/andrew/code/gitlab/test_code/"
# windows cmd
set YOK_PATH="C:\path\to\your\directory"
# windows powershell
$env:YOK_PATH = "C:\path\to\your\directory"

cargo clean
cargo r
```