use std::{mem, path::PathBuf, str::FromStr};
/// include dir at compile time
/// in default die path is current dir
///
/// `export YOK_PATH="/home/andrew/code/gitlab/test_code/"`
///
/// you can also set YOK_PATH env before cargo run
///
/// If the dir path is not the path you set, run `cargo clean` before `cargo run`
/// ```rust
///   use yok::include_dir;
///   use yok::Dir;
///   use yok::DirEntry;
///   use yok::Bytes;
///   const DATA:&[u8] = include_dir();
///   let dir:Dir = DATA.into_dir();
///   for entry in &dir.data{
///       if entry.is_file{
///           println!("{}",String::from_utf8_lossy(&entry.contents));
///       }else if entry.is_dir {
///           println!("{}",entry.path);
///       }
///   }
///   dir.extract("./demo");
/// ```
///
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct DirEntry {
    pub path: String,
    pub is_dir: bool,
    pub is_file: bool,
    pub contents: Vec<u8>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Dir {
    pub data: Vec<DirEntry>,
}
pub trait Bytes {
    fn as_bytes(&self) -> Vec<u8>;
    fn from_bytes(&self) -> Dir;
    fn into_bytes(&self) -> Vec<u8>;
    fn into_dir(&self) -> Dir;
}

impl Bytes for [u8] {
    fn as_bytes(&self) -> Vec<u8> {
        self.to_vec()
    }
    fn into_bytes(&self) -> Vec<u8> {
        self.to_vec()
    }

    fn from_bytes(&self) -> Dir {
        Dir::from_bytes(self).expect("from bytes error")
    }
    fn into_dir(&self) -> Dir {
        Dir::from_bytes(self).expect("from bytes error")
    }
}

impl Bytes for Dir {
    fn as_bytes(&self) -> Vec<u8> {
        Dir::as_bytes(&self)
    }
    fn into_bytes(&self) -> Vec<u8> {
        Dir::as_bytes(&self)
    }

    fn from_bytes(&self) -> Dir {
        self.clone()
    }
    fn into_dir(&self) -> Dir {
        self.clone()
    }
}

// Implement as_bytes() for Dir
impl Dir {
    pub fn as_bytes(&self) -> Vec<u8> {
        // Calculate the total size of the serialized data
        let mut size = mem::size_of::<u32>(); // Size of the length prefix

        for entry in &self.data {
            size += mem::size_of::<u32>(); // Size of the entry size prefix
            size += entry.path.as_bytes().len();
            size += mem::size_of::<bool>() * 2; // Sizes of is_dir and is_file fields
            size += mem::size_of::<u32>(); // Size of content size prefix
            size += entry.contents.len();
        }

        let mut bytes = Vec::with_capacity(size);

        // Write the length prefix
        bytes.extend((self.data.len() as u32).to_le_bytes().iter());

        for entry in &self.data {
            // Write the size prefix for this entry
            bytes.extend((entry.path.as_bytes().len() as u32).to_le_bytes().iter());

            // Write the entry data
            bytes.extend(entry.path.as_bytes().iter());
            bytes.extend(&[entry.is_dir as u8, entry.is_file as u8]);
            bytes.extend((entry.contents.len() as u32).to_le_bytes().iter());
            bytes.extend(entry.contents.iter());
        }

        bytes
    }
}

// Implement from_bytes() for Dir
impl Dir {
    pub fn from_bytes(bytes: &[u8]) -> Option<Self> {
        let mut cursor = 0;
        let len_prefix_size = mem::size_of::<u32>();

        if bytes.len() < len_prefix_size {
            return None;
        }

        let len = u32::from_le_bytes([
            bytes[cursor],
            bytes[cursor + 1],
            bytes[cursor + 2],
            bytes[cursor + 3],
        ]) as usize;

        cursor += len_prefix_size;

        let mut data = Vec::with_capacity(len);

        for _ in 0..len {
            if bytes.len() < cursor + len_prefix_size {
                return None;
            }

            let entry_size = u32::from_le_bytes([
                bytes[cursor],
                bytes[cursor + 1],
                bytes[cursor + 2],
                bytes[cursor + 3],
            ]) as usize;

            cursor += len_prefix_size;

            let end_pos = cursor + entry_size;

            if bytes.len() < end_pos {
                return None;
            }

            let path = String::from_utf8(bytes[cursor..end_pos].to_vec()).ok()?;
            cursor = end_pos;

            if bytes.len() < cursor + 2 {
                return None;
            }

            let is_dir = bytes[cursor] != 0;
            let is_file = bytes[cursor + 1] != 0;
            cursor += 2;

            if bytes.len() < cursor + len_prefix_size {
                return None;
            }

            let content_size = u32::from_le_bytes([
                bytes[cursor],
                bytes[cursor + 1],
                bytes[cursor + 2],
                bytes[cursor + 3],
            ]) as usize;

            cursor += len_prefix_size;

            if bytes.len() < cursor + content_size {
                return None;
            }

            let contents = bytes[cursor..cursor + content_size].to_vec();
            cursor += content_size;

            data.push(DirEntry {
                path,
                is_dir,
                is_file,
                contents,
            });
        }

        Some(Dir { data })
    }
}
fn get_new_path(path: &str) -> String {
    let yok_path = match std::env::var("YOK_PATH") {
        Ok(value) => value,
        Err(_) => ".".to_string(),
    };
    let path = path.to_string().replace("\\", "/").to_string();
    if yok_path != "."{
        return path;
    }else {
        return path;
    }
}
#[allow(warnings)]

impl Dir {
    /// include dir at compile time
    /// in default die path is current dir
    ///
    /// `export YOK_PATH="/home/andrew/code/gitlab/test_code/"`
    ///
    /// you can also set YOK_PATH env before cargo run
    ///
    /// If the dir path is not the path you set, run `cargo clean` before `cargo run`
    /// ```rust
    ///   use yok::include_dir;
    ///   use yok::Dir;
    ///   use yok::DirEntry;
    ///   use yok::Bytes;
    ///   const DATA:&[u8] = include_dir();
    ///   let dir:Dir = DATA.into_dir();
    ///   for entry in &dir.data{
    ///       if entry.is_file{
    ///           println!("{}",String::from_utf8_lossy(&entry.contents));
    ///       }else if entry.is_dir {
    ///           println!("{}",entry.path);
    ///       }
    ///   }
    ///   dir.extract("./demo");
    /// ```
    ///
    pub fn extract(&self, base_dir: impl ToString) ->std::io::Result<()>{
        std::fs::create_dir_all(base_dir.to_string()).expect("create to dir error");
        for entry in self.data.clone().into_iter() {
            if entry.is_dir {
                let path = PathBuf::from_str(&base_dir.to_string()).expect("parse to to PathBuf error");
                let path = path.join(get_new_path(&entry.path.to_string()));
                std::fs::create_dir_all(path.clone()).expect("create to dir error");
                if cfg!(target_os = "windows") {
                    use std::os::unix::fs::PermissionsExt;
                    let permissions = std::fs::Permissions::from_mode(0o777);
                    std::fs::set_permissions(path.clone(), permissions).unwrap();
                }else{
                    use std::os::unix::fs::PermissionsExt;
                    let permissions = std::fs::Permissions::from_mode(0o777);
                    std::fs::set_permissions(path.clone(), permissions).unwrap();
                }
            }
        }

        for entry in self.data.clone().into_iter() {
            if entry.is_file {
                use std::fs::{self, File, OpenOptions};
                use std::io::prelude::*;
                use std::os::unix::fs::OpenOptionsExt;
                let path = PathBuf::from_str(&base_dir.to_string()).expect("parse to to PathBuf error");
                let path = path.join(get_new_path(&entry.path.to_string()));
                if !path.exists(){
                    let mut open_options = OpenOptions::new();
                    open_options.mode(0o777); // Set read/write permission for owner, read-only for others
                    let file = open_options.create(true).write(true).open(path).unwrap();
                    let mut file_writer = std::io::BufWriter::new(file);
                    file_writer.write_all(&entry.contents).unwrap();
                }
            }
        }
        Ok(())
    }
}

/// include dir at compile time
/// in default die path is current dir
///
/// `export YOK_PATH="/home/andrew/code/gitlab/test_code/"`
///
/// you can also set YOK_PATH env before cargo run
///
/// If the dir path is not the path you set, run `cargo clean` before `cargo run`
/// ```rust
///   use yok::include_dir;
///   use yok::Dir;
///   use yok::DirEntry;
///   use yok::Bytes;
///   const DATA:&[u8] = include_dir();
///   let dir:Dir = DATA.into_dir();
///   for entry in &dir.data{
///       if entry.is_file{
///           println!("{}",String::from_utf8_lossy(&entry.contents));
///       }else if entry.is_dir {
///           println!("{}",entry.path);
///       }
///   }
///   dir.extract("./demo");
/// ```
///
pub const fn include_dir() -> &'static [u8] {
    include_bytes!("../../.yok")
}
