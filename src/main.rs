#[allow(warnings)]
fn main() {
    use yok::include_dir;
    use yok::Bytes;
    use yok::Dir;
    use yok::DirEntry;
    const DATA: &[u8] = include_dir();
    let dir: Dir = DATA.into_dir();
    for entry in &dir.data {
        if entry.is_file {
            // println!("{}", String::from_utf8_lossy(&entry.contents));
        } else if entry.is_dir {
            // println!("{}", entry.path);
        }
    }
    dir.extract("./path");

}

